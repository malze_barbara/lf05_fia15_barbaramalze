//Schreibe ein Programm, das f�r alle Zahlen zwischen 1 und 200 testet:
// ob sie durch 7 teilbar sind.
// nicht durch 5 aber durch 4 teilbar sind.
//Lasse jeweils die Zahlen, auf die die Bedingungen zutreffen ausgeben.

public class ModuluForLoop {

	public static void main(String[] args) {
		System.out.println("Von allen Zahlen von 1 bis 200 sind diese Zahlen durch 7 und 4, aber nicht durch 5 teilbar:");
		
		for (int i = 1; i <= 200; i++) { 
			if (i % 7 == 0 && i % 4 == 0 && i != 5) {
				System.out.println(i);
				
			}
			
		}

	}

}
