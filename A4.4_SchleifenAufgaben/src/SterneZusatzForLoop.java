//Schreiben Sie ein Programm, das eine von Ihnen vorgegebene Anzahl von Sternen (*) in
//Form eines Dreiecks auf dem Bildschirm ausgibt.

import java.util.Scanner;

public class SterneZusatzForLoop {

	public static void main(String[] args) {
		
		int n;
		Scanner tastatur = new Scanner(System.in);	
		System.out.println("Geben Sie x (Anzahl von Sternen) ein: ");
		n = tastatur.nextInt();

		for (int i = 1; i <= n; i++) { 
			for (int stern=1; stern<=i; stern++) {
				System.out.print("*");
			}
		System.out.println();
			
		}

	}

}
