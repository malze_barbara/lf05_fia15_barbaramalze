//Geben Sie in der Konsole die Summe der Zahlenfolgen aus. Ermöglichen Sie es dem
//Benutzer die Zahl n festzulegen, welche die Summierung begrenzt.

import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n;	
		
		Scanner tastatur = new Scanner(System.in);	
		System.out.println("Geben Sie n ein: ");
		n = tastatur.nextInt();
		
		int zaehler = 0;
		int summe = 0;
		while (zaehler <= n){
			summe += zaehler;
			zaehler++;			
		}
		System.out.print(summe);
		

	}

}
