//Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend (bzw. von n bis 1
//herunterz�hlend) aus. Erm�glichen Sie es dem Benutzer die Zahl n festzulegen. Nutzen Sie
//zur Umsetzung eine for-Schleife.
//a) 1, 2, 3, �, n 		for-Schleife
//b) n, �, 3, 2, 1 		for-Schleife

import java.util.Scanner;

public class Z�hlen {

	public static void main(String[] args) {
		
	int n;	
	Scanner tastatur = new Scanner(System.in);	
	System.out.println("Geben Sie n ein: ");
	n = tastatur.nextInt();
	
	System.out.println("Bis n hochz�hlen: ");
	for (int i = 1; i <= n; i++) { //a) heraufz�hlen
		System.out.println(i);
	}
	System.out.println("Von n runterz�hlen: ");
	for (int i = n; i >  0; i--) { //b) herunterz�hlen
		System.out.println(i);
	}
	
	
	}
	
	

}
