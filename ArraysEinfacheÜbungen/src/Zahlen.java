//Schreiben Sie ein Programm �Zahlen�, in welchem ein ganzzahliges Array der L�nge 10 deklariert wird. 
//Anschlie�end wird das Array mittels Schleife mit den Zahlen von 0 bis 9 gef�llt. Zum Schluss geben Sie 
//die Elemente des Arrays wiederum mit einer Schleife auf der Konsole aus.
public class Zahlen {

	public static void main(String[] args) {
		
		System.out.println("Zahlen von 0 bis 9:");
		
		int[] feld = new int[10]; //ganzzahlig, L�nge 10
		
		int x = 0;
		for (int i = 0; i< feld.length; i++) { //mit Schleife von 0 bis 9 f�llen
			feld[i] = x;
			x++;
			
		}
		for (int i = 0; i< feld.length; i++) {
			System.out.println(feld[i]); //Ausgabe mit Schleife
		}
		
		
		

	}

}
