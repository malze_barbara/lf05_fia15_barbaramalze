//Im Programm �Palindrom� werden �ber die Tastatur 5 Zeichen eingelesen und in einem
//geeigneten Array gespeichert. Ist dies geschehen, wird der Arrayinhalt in umgekehrter 
//Reihenfolge (also von hinten nach vorn) auf der Konsole ausgegeben.

import java.util.Scanner;

public class Palindrom {

	public static void main(String[] args) {

		char[] zeichen = new char[5];
		
		Scanner tastatur = new Scanner(System.in);

		for(int i = 0; i < zeichen.length; i++) {
			System.out.print((i + 1) + ". Zeichen: ");
			zeichen[i] = tastatur.next().charAt(0);
		}
		
		for(int i = zeichen.length - 1; i >= 0 ; i--) {
			System.out.print(zeichen[i]);
		}
		System.out.println();
	}
}
