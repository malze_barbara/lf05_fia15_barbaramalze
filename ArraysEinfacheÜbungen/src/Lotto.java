//Jetzt wird Lotto gespielt. In der Klasse �Lotto� gibt es ein ganzzahliges Array, welches 6 Lottozahlen 
//von 1 bis 49 aufnehmen kann. Konkret sind das die Zahlen 3, 7, 12, 18, 37 und 42. Tragen Sie diese im 
//Quellcode fest ein. 

public class Lotto {

	public static void main(String[] args) {

		int[] lottoziehung = new int[6];
		
		lottoziehung[0] = 3;
		lottoziehung[1] = 7;
		lottoziehung[2] = 12;
		lottoziehung[3] = 18;
		lottoziehung[4] = 37;
		lottoziehung[5] = 42;

		System.out.print("[  ");
		for(int i = 0; i < lottoziehung.length; i++) {
			System.out.print(lottoziehung[i] + "  ");
		}		
		System.out.println("]");
		
		
		int gesuchteZahl;
		boolean istEnthalten;
		
		gesuchteZahl = 12;
		istEnthalten = false;
		for(int i = 0; i < lottoziehung.length; i++) {
		   if(lottoziehung[i] == gesuchteZahl) {
			   istEnthalten = true;
			   break;
		   }
		}
		if(istEnthalten) {
			   System.out.println("Die Zahl " + gesuchteZahl +  " ist enthalten.");			
		}
		else {
			   System.out.println("Die Zahl " + gesuchteZahl +  " ist nicht enthalten.");						
		}
		
		gesuchteZahl = 13;
		istEnthalten = false;
		for(int i = 0; i < lottoziehung.length; i++) {
		   if(lottoziehung[i] == gesuchteZahl) {
			   istEnthalten = true;
			   break;
		   }
		}
		if(istEnthalten) {
			System.out.println("Die Zahl " + gesuchteZahl +  " ist in der Ziehung enthalten.");			
		}
		else {
			System.out.println("Die Zahl " + gesuchteZahl +  " ist nicht in der Ziehung enthalten.");						
		}
	}
}
