//Das zu schreibende Programm �UngeradeZahlen� ist �hnlich der Aufgabe 1. Sie deklarieren wiederum ein Array mit 10 Ganzzahlen. 
//Danach f�llen Sie es mit den ungeraden Zahlen von 1 bis 19 und geben den Inhalt des Arrays �ber die Konsole aus 
//(Verwenden Sie Schleifen!).

public class UngeradeZahlen {

	public static void main(String[] args) {
		
System.out.println(" Ungerade Zahlen von 0 bis 19:");
		
		int[] feld = new int[10]; //ganzzahlig, L�nge 10
		
		int x = 1;
		for (int i = 0; i< feld.length; i++) { //mit Schleife von ungerade Zahlen von 1 bis 19 f�llen
			feld[i] = x;
			x++;
			if (x % 2 == 0) {
				x++;
			}
			
		}
		for (int i = 0; i< feld.length; i++) {
			System.out.println(feld[i]); //Ausgabe mit Schleife
		}

	}

}
