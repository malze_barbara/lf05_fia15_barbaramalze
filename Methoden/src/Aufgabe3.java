//Erstellen Sie eine Hauptmethode und entsprechende zugeh�rige Methoden, die die folgenden
//Volumen berechnet und entsprechend ausgibt.
//a) W�rfel: V = a * a * a
//b) Quader: V = a * b * c
//c) Pyramide: V = a * a * h / 3
//d) Kugel: V = 4/3 * r� * Pi. 

public class Aufgabe3 {

	public static void main(String[] args) {
		System.out.println("Volumenberechnung:");
		//a)
		double x = 2.5;
		double y = 2.5;
		double z = 2.5;
		double i = w�rfel(x,y,z);
		System.out.println("Volumen des W�rfels: "+ i);
		//b
		double d = 5.5;
		double e = 4.5;
		double f = 3.5;
		double g = quader(d,e,f);
		System.out.println("Volumen des Quaders: " + g);
		//c
		double h = 4.5;
		double i2 = 4.5;
		double j = 6.8;
		double k = 3.0;
		double l = pyramide(h, i2, j, k);
		System.out.println("Volumen der Pyramide: " + l);
		//d
		double m = 4.0;
		double n = 3.0;
		double o = 12.0;
		double p = 3.14;
		double q = kugel(m, n, o, p);
		System.out.println("Volumen der Kugel: " + q);
		
	}
	public static double w�rfel(double a, double b, double c){
		return(a * b * c);
	}
	public static double quader(double a, double b, double c){
		return (a * b * c);
	}
	public static double pyramide(double a, double b, double c, double d){
		return(a * b * c / d);
	}
	public static double kugel(double a, double b, double c, double d){
		return (a / b * c * d);
	}

}

