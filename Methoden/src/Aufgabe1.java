//�berlegen Sie, was der folgende Code in den folgenden Klassen macht. Was erwartest Sie als Ausgabe?
//F�hren Sie den Code aus und vergleichen Sie das Ergebnis mit Ihren Erwartungen.

public class Aufgabe1 {
public static void main(String[] args) {
ausgabe(1, "Mana");
ausgabe(2, "Elise");
ausgabe(3, "Johanna");
ausgabe(4, "Felizitas");
ausgabe(5, "Karla");
System.out.println(vergleichen(1, 2));
System.out.println(vergleichen(1, 5));
System.out.println(vergleichen(3, 4));
}
public static void ausgabe(int zahl, String name) {
System.out.println(zahl + ": " + name);
}
public static boolean vergleichen(int arg1, int arg2) {
return (arg1 + 8) < (arg2 * 3);
}
}

//ausgabe: es wird die angegebene Zahl, einen Doppelpunkt & den angegebenen Namen ausgegeben
//vergleichen: die erste Zahl wird mit der zweiten Zahl verglichen: Ist die erste Zahl plus 8 kleiner
  //als die zweite Zahl mal 3? Wenn ja: true; Wenn nein: false