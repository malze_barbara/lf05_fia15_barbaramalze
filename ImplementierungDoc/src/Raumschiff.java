/**Das Array-List-Package*/import java.util.ArrayList;

/** Klasse "Raumschiff" */public class Raumschiff {

  /**Anzahl der Photonentorpedos.*/private int photonentorpedoAnzahl; 
  /**Energieversorgung in Prozent*/private int energieversorgungInProzent;
  /**Schilde in Prozent*/private int schildeInProzent;
  /**H�lle in Prozent*/private int huelleInProzent;
  /**Lebenserhaltungssysteme in Prozent.*/private int lebenserhaltungssystemeInProzent;
  /**Anzahl Androiden*/private int androidenAnzahl;
  /**Name von Raumschiff*/private String schiffsname;
  /**Der Broadcast-Kommunikator*/private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
  /**Das Ladungsverzeichnis*/private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>(); 
  
  
  // Konstruktoren
  /**Parameterloser Konstruktor.
   * */public Raumschiff() {
    
  }
  
  /**Parametisierter Konstruktor.*/public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent,
                int zustandSchildeInProzent, int zustandHuelleInProzent,
                int zustandLebenserhaltungssystemeInProzent,
                int anzahlDroiden, String schiffsname) {
    this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    this.energieversorgungInProzent = energieversorgungInProzent;
    this.schildeInProzent = zustandSchildeInProzent;
    this.huelleInProzent = zustandHuelleInProzent;
    this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
    this.androidenAnzahl = anzahlDroiden;
    this.schiffsname = schiffsname;
  }
  
  
  // getter & setter
  /**Gibt Wert f�r Anzahl der Photonentorpedos zur�ck.
   * @return Anzahl der Photonentorpedos
   * */public int getPhotonentorpedoAnzahl() {
    return photonentorpedoAnzahl;
  }

  /**Setzt Wert f�r Photonentorpedo-Anzahl.
   * */public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
    this.photonentorpedoAnzahl = photonentorpedoAnzahl;
  }

  /**Gibt Wert f�r Energieversorgung zur�ck.
   * @return Energieversorgung in Prozent 
   * */public int getEnergieversorgungInProzent() {
    return energieversorgungInProzent;
  }

  /**Setzt Wert f�r Energieversorgung.
   * */public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
    this.energieversorgungInProzent = energieversorgungInProzent;
  }

  /**Gibt Wert f�r Schilde zur�ck.
   * @return Schilde in Prozent
   * */public int getSchildeInProzent() {
    return schildeInProzent;
  }

  /**Setzt Wert f�r Schilde.
   * */public void setSchildeInProzent(int schildeInProzent) {
    this.schildeInProzent = schildeInProzent;
  }

  /**Gibt Wert f�r H�lle zur�ck.
   * @return H�lle in Prozent
   * */public int getHuelleInProzent() {
    return huelleInProzent;
  }

  /**Setzt Wert f�r H�lle.
   * */public void setHuelleInProzent(int huelleInProzent) {
    this.huelleInProzent = huelleInProzent;
  }

  /**Gibt Wert f�r Lebenserhaltungssysteme zur�ck.
   * @return Lebenserhaltungssysteme in Prozent
   * */public int getLebenserhaltungssystemeInProzent() {
    return lebenserhaltungssystemeInProzent;
  }

  /**Setzt Wert f�r Lebenserhaltungssysteme.
   * */public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
    this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
  }

  /**Gibt Wert f�r Androiden-Azahl zur�ck.
   * @return Anzahl der Androiden 
   * */public int getAndroidenAnzahl() {
    return androidenAnzahl;
  }

  /**Setzt Wert f�r Androiden-Anzahl.
   * */public void setAndroidenAnzahl(int androidenAnzahl) {
    this.androidenAnzahl = androidenAnzahl;
  }
  /**Gibt Wert f�r Raumschiffsnamen zur�ck.
   * @return Name des Raumschiffs
   * */public String getSchiffsname() {
    return schiffsname;
  }

  /**Setzt Wert f�r Raumschiffsnamen.
   * */public void setSchiffsname(String schiffsname) {
    this.schiffsname = schiffsname;
  }
  
  
  // methoden
  /**Alle Attribute mit dazugeh�rigen Namen werden ausgegeben.
   * */public void zustandRaumschiff() {
	  System.out.println("Photonentorpedoanzahl:" + photonentorpedoAnzahl);
	  System.out.println("Enegieversorgung in Prozent:" + energieversorgungInProzent);
	  System.out.println("Schilde in Prozent:" + schildeInProzent);
	  System.out.println("H�lle in Prozent:" + huelleInProzent);
	  System.out.println("Lebenserhaltungssysteme in Prozent:" + lebenserhaltungssystemeInProzent);
	  System.out.println("Androiden-Anzahl:" + androidenAnzahl);
	  System.out.println("Schiffsname:" + schiffsname);
	  System.out.println("Broadcast-Kommunikator:" + broadcastKommunikator);
	  System.out.println("Ladungsverzeichnis" + ladungsverzeichnis);

  }
  
  /**Ausgabe: gesammtes Ladungsverezeichnis"*/public void printLadungsverzeichnis() {
	  System.out.println("Ladungsverzeichnis" + ladungsverzeichnis);
  }
  
  /**F�gt neue Ladung hinzu*/public void addLadung(Ladung neueLadung) {
    ladungsverzeichnis.add(neueLadung);
  }
  
  /** Voraussetzung: keine Photonentorpedos.
   * Folge: Ausgabe "-=*Click*=-.
   * Ansonsten werden Photonentorpedos um 1 reduziert,
   * Nachricht "Photonentorpedo abgeschossen" wird ausgegeben
   * und die Methode "treffer(r)" wird aufgerufen
   * */public void photonentorpedoSchiessen(Raumschiff r) {
	  if (photonentorpedoAnzahl == 0) {
		  System.out.println("-=*Click*=-");
	      }else {
			  photonentorpedoAnzahl --;
			  System.out.println("Photonentorpedo abgeschossen");
			  treffer(r);
		  }
		  
	  }
  
  
  
  /** Voraussetzung: Energieversorgung unter 50%.
   * Folge: Nachricht "-=*Click*=- wird ausgegeben.
   * Ansonsten: Energieversorgung wird um 50 reduziert, die Nachricht
   * "Phaserkanone abgeschossen" wird ausgegeben und die 
   * Methode "treffer(r) wird aufgerufen.
   * */public void phaserkanoneSchiessen(Raumschiff r) {
	  if (energieversorgungInProzent < 50 ) {
		  System.out.println("-=*Click*=-");
	  }else {
		  energieversorgungInProzent =- 50;
		  System.out.println("Phaserkanone abgeschossen");
		  treffer(r);
	  }
  }
  
  /**Der Schiffsname wird mit der Nachricht "wurde getroffen" ausgegeben.
   * */private void treffer(Raumschiff r) {
	  System.out.println(schiffsname + "wurde getroffen");
  }
  
  /**Mit dem Broadcast-Kommunikator wird eine Nachricht an alle gesendet
   * */public void nachrichtAnAlle(String message) {
	  System.out.println("Nachricht:" + broadcastKommunikator);
  }
  
  
  /**Die Photonentorpedos werden geladen.
   * */public void photonentorpedosLaden(int anzahlTorpedos) {
    
  }
  
  /**Es wird eine Reparatur durchgef�hrt.
   * */public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,
                                      boolean schiffshuelle, int anzahlDroiden)  {
    
  }
  
 /**Das Ladungsverzeichnis wird ausgegeben.
  * */ public void ladungsverzeichnisAusgeben() {
    
  }
  
  /**Das Ladungsverzeichnis wird aufger�umt.
   * */public void ladungsverzeichnisAufraeumen() {
    
  }
}
