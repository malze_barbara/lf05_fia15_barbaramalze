public class Temperaturtabelle{

	public static void main(String[] args) {
		        //Aufgabe 1: Programm schreiben, dass angegebenen Text ausgibt:
				String s = "**";                      
				String l = "*";
				System.out.printf("%9s", s); 
				System.out.printf("\n%5s %6s", l, l);
				System.out.printf("\n%5s %6s", l, l);
				System.out.printf("\n%9s", s);
				System.out.println();
			
				//Aufgabe 2: Programm schreiben, dass einen anderen angegebenen Text ausgibt:
				System.out.printf("\n%-5s %5s %19s %4s", "0!", "=", "=", "1");
				System.out.printf("\n%-5s %5s %-17s %-2s %3s", "1!", "=", "1", "=", "1");
				System.out.printf("\n%-5s %5s %-17s %-2s %3s", "2!", "=", "1 * 2", "=", "2");
				System.out.printf("\n%-5s %5s %-17s %-2s %3s", "3!", "=", "1 * 2 * 3", "=", "6");
				System.out.printf("\n%-5s %5s %-17s %-2s %3s", "4!", "=", "1 * 2 * 3 * 4", "=", "24");
				System.out.printf("\n%-5s %5s %-17s %-2s %3s", "5!", "=", "1 * 2 * 3 * 4 * 5", "=", "120");
				System.out.println();

				//Aufgabe 3: ein anderes Programm schreiben, welches einen angegebenen Text ausgibt:
				System.out.printf("\n%1s %-10s %-9s", "Fahrenheit", "|", "Celsius");
				System.out.printf("\n%1s", "----------------------");
				System.out.printf("\n%1s %8s %9s", "-18", "|", "-28.89");
				System.out.printf("\n%1s %8s %9s", "-10", "|", "-23.33");
				System.out.printf("\n%1s %9s %9s", "+0", "|", "-17.78");
				System.out.printf("\n%1s %8s %9s", "+20", "|", "-6.67");
				System.out.printf("\n%1s %8s %9s", "+30", "|", "-1.11");
				
 }

}
