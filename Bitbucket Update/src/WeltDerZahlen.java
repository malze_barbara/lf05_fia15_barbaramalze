public class WeltDerZahlen {
	public static void main(String[] args) {
		System.out.println("Die Welt der Zahlen!");
	    	    
	    	int anzahlPlaneten = 8;			    
	        long anzahlSterne = 400000000l; 			    	    
	        int bewohnerBerlin = 3645000;			    		    
	        int alterTage = 7670;			    	
	        int gewichtKilogramm = 150000;			    
	        int flaecheGroessteLand = 17100000;			    		    
	        float flaecheKleinsteLand = 0.44f;
	       
	       //Ausgabe:
	        System.out.println("Anzahl der Planeten in unserem Sonnensystem:" + anzahlPlaneten);
	        System.out.println("Anzahl der Sterne in der Milchstrasse:" + anzahlSterne);
	        System.out.println("Wie viele Einwohner hat Berlin?:" + bewohnerBerlin);
	        System.out.println("Wie alt bin ich in Tagen?:" + alterTage);
	        System.out.println("Wie viel wiegt das schwerste Tier der Welt?:" + gewichtKilogramm);
	        System.out.println("Wie viel Quadratkilometer hat das groesste Land der Erde?:" + flaecheGroessteLand);
	        System.out.print("Wie gross ist das kleinste Land der Erde?:" + flaecheKleinsteLand+ "Quadratkilometer");

    }
}