//Aufgabe: Erstellen Sie f�r die einzelnen Ablaufphasen des Fahrkartenautomaten die vier Methoden:
//fahrkartenbestellungErfassen
//fahrkartenBezahlen
//fahrkartenAusgeben
//rueckgeldAusgeben

import java.util.Scanner;

public class Ablaufphasen {

	public static void main(String[] args) {
		//Infos:
		System.out.println("Dies ist ein Fahrkartenautomat.");
		System.out.println("Ein Ticket kostet 2,50�");
		
		//Bestellung erfassen:
		Scanner myScanner = new Scanner (System.in);
		System.out.println("Wie viele Tickets m�chten Sie kaufen? ");
		int bestellung = myScanner.nextInt();
		double ticketPreis = 2.50;
		double gesamtPreis = fahrkartenbestellungErfassen(ticketPreis, bestellung);
		System.out.println("Ihr Preis betr�gt: " + gesamtPreis + "�");
		
		//Zahlen der Fahrkarten:
		Scanner myScanner2 = new Scanner (System.in);
		System.out.println("Bitte geben Sie " + gesamtPreis + " � oder mehr ein: ");
		double m�nzen = myScanner.nextDouble();
		double betrag = m�nzen;
		double zuZahlen = fahrkartenBezahlen(m�nzen, betrag);
		System.out.println("Gezahlter Betrag: " + zuZahlen + "�");
		//System.out.println("Vielen Dank f�r Ihren Einkauf!");
		
		System.out.println("Die Zahlung von " + gesamtPreis + " � wurde best�tigt.");
		
		//Ausgabe der Fahrkarten
		double anzahl = bestellung;
		double aus = anzahl;
		double ausgabe = fahrkartenAusgeben(bestellung, aus );
		System.out.println("Sie haben " + ausgabe + " Fahrkarten erhalten.");
		
		//Ausgabe des Restgeldes
		double gesamt = gesamtPreis;
		double bezahlt = m�nzen;
		double restgeld = rueckgeldAusgeben(gesamt, bezahlt);
		System.out.println("Ihr Restgeld betr�gt: " + restgeld + " �");
		System.out.println("Sie haben Ihr Restgeld nun erhalten.");
		
		//Abschluss
		System.out.println("Vielen Dank f�r Ihren Einkauf!");
		System.out.println("Auf Wiedersehen!");
		
	}
	
	
	public static double fahrkartenbestellungErfassen(double a, int b) {
		return(a * b);
		
	}
	
	public static double fahrkartenBezahlen(double a, double b) {
		return(a = b);
		
	}
	
	public static double fahrkartenAusgeben(double a, double b) {
		return(a = b);
	}

	public static double rueckgeldAusgeben(double a, double b) {
		return(a - b);
	}
	
	
}
