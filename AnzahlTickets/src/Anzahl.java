//Aufgabe: A2.5: Anzahl der Tickets hinzuf�gen
//Einzelaufgaben befinden sich ganz unten.
import java.util.Scanner;
public class Anzahl {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
	      
	       double zuZahlenderBetrag; 
	       double eingezahlterGesamtbetrag;
	       double eingeworfeneM�nze;
	       double r�ckgabebetrag;
	       int anzahlTickets;

	       System.out.print("Ticketpreis (EURO): ");
	       zuZahlenderBetrag = tastatur.nextDouble();
	       
	     //Anzahl der Tickets (Aufgabe 3)
	       anzahlTickets = 0;
	       System.out.println("Erw�nschte Anzahl der Tickets: ");
	       anzahlTickets = tastatur.nextInt();
	       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
	     
	       // Geldeinwurf
	       // -----------
	       eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
	       

	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	    
		

	}

}

//Aufgabe 1: Notieren Sie die Variablen, die im Quelltext deklariert werden, und die Datentypen dieser Variablen.
//zuZahlenderBetrag: double, eingezahlterGesamtbetrag: double; eingeworfeneM�nze: double; r�ckgabebetrag: double

//Aufgabe 2: Welche Operationen werden mit den Variablen ausgef�hrt?
//zuZahlenderBetrag: einfache Zuweisung (Scanner: "tastatur"), Differenz zu eingezahlterGesamtbetrag
//eingezahlterGesamtbetrag: einfache Zuweisung (0.0), Kleiner (als zuZahlenerBetrag), Additionszuweisung (eingeworfeneM�nze)
//eingeworfeneM�nze: einfacheZuweisung(Scanner: "tastatur"), 
//r�ckgabebetrag: einfache Zuweisung (eingezahlterGesamtbetrag) & Differenz (zuZahlenderBetrag), Gr��er gleich (2.0), diverse Subtraktionszuweisungen (1.0, 0.5 ...)

//Aufgabe 3: Erg�nzen Sie das Programm so, dass nach Eingabe des Einzelpreises die Anzahl der Fahrkarten eingegeben werden kann.

//Aufgabe 4: Programm anschlie�end testen und wenn alle Fehler korrigiert sind, die aktuelle Version in das Repository �bertragen.
//Ist somit �bertragen!

//Aufgabe 5: Begr�nden Sie Ihre Entscheidung f�r die Wahl des Datentyps.
//Ich habe "int" als Datentyp gew�hlt, weil "int" ein Datentyp f�r Zahlen ohne Kommastellen, also f�r ganze Zahlen ist und Ticketanzahlen auch Zahlen sind, man Tickets aber nur ganz kaufen kann.

//Aufgabe 6: Erl�utern Sie detailliert, was bei der Berechnung des Ausdrucks anzahl * einzelpreis passiert.
//Bei der Berechnung anzahl * einzelpreis wird die Anzahl der Tickets mit dem Einzelpreis multipliziert, um somit
//den zu zahlenden Betrag berechnet zu bekommen. Mit diesem Ergebnis kann nun weiter gerechnet werden, wenn man
//die eingeworfene M�nze eingibt. Das Ergebnis ist also ein wichtiges Zwischenergebnis um die Anzahl der Tickets
//mit in den Fahrkartenautomaten zu integrieren.
