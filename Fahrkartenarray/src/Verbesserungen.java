//Aufgabe 1: Fahrkarteninformationen sollen innerhalb des Quelltexts an einer Stelle (Methode) verwaltet werden.
//Ung�ltige Geldbetr�ge werden beim Einwerfen erkannt und es erfolgt eine angemessene Reaktion (done).
//Methoden & Variablen werden konsistent benannt (done).
//Interaktion mit Benutzer optimieren (done).

import java.util.Scanner;

public class Verbesserungen {
    	
    public static double fahrkartenbestellungErfassen() {
    	
    	
        int anzahlTickets;  
        double ticketPreis = 0;
        Scanner tastatur = new Scanner(System.in);
        int ticketBezeichnung;
        
        	System.out.println("Dies ist ein Fahrkartenautomat. Viel Spa� bei Ihrer Bestellung!");
        	System.out.println("M�gliche Fahrkarten: ");
            String[] fahrkartenbezeichnung = new String[] {"Einzelfahrschein Berlin AB (1)", "Einzelfahrschein Berlin BC (2)", "Einzelfahrschein Berlin ABC (3)", "Kurzstrecke(4)\n", "Tageskarte Berlin AB (5)", "Tageskarte Berlin BC (6)", "Tageskarte Berlin ABC (7)", "Kleingruppen Tageskarte Berlin AB (8)\n", "Kleingruppen Tageskarte Berlin BC (9)", "Tageskarte Berlin ABC (10)"};
            for (int i = 0; i < fahrkartenbezeichnung.length; i++) {
            } 
            for (int i = 0; i < fahrkartenbezeichnung.length; i++) {
              System.out.print(fahrkartenbezeichnung[i] + " ");
            }
            System.out.println();
          
    
            System.out.println("Preise:" );
    		double[] fahrkartenpreise = new double[] {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    		for (int i = 0; i < fahrkartenpreise.length; i++) {
    		} 
    		for (int i = 0; i < fahrkartenpreise.length; i++) {
    			System.out.print(fahrkartenpreise[i] + " ");
    		}
    		System.out.println();
    		
    		
    		System.out.println("W�hlen Sie Ihre Fahrkarte (1-10)");
    		ticketBezeichnung = tastatur.nextInt();
    		
    		if (ticketBezeichnung == 1) {
    			ticketPreis = fahrkartenpreise[0];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[0] + "�");
    		}
    		if (ticketBezeichnung == 2) {
    			ticketPreis = fahrkartenpreise[1];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[1] + "�");
    		}
    		if (ticketBezeichnung == 3) {
    			ticketPreis = fahrkartenpreise[2];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[2] + "�");
    		}
    		if (ticketBezeichnung == 4) {
    			ticketPreis = fahrkartenpreise[3];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[3] + "�");
    		}
    		if (ticketBezeichnung == 5) {
    			ticketPreis = fahrkartenpreise[4];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[4] + "�");
    		}
    		if (ticketBezeichnung == 6) {
    			ticketPreis = fahrkartenpreise[5];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[5] + "�");
    		}
    		if (ticketBezeichnung == 7) {
    			ticketPreis = fahrkartenpreise[6];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[6] + "�");
    		}
    		if (ticketBezeichnung == 8) {
    			ticketPreis = fahrkartenpreise[7];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[7] + "�");
    		}
    		if (ticketBezeichnung == 9) {
    			ticketPreis = fahrkartenpreise[8];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[8] + "�");
    		}
    		if (ticketBezeichnung == 10) {
    			ticketPreis = fahrkartenpreise[9];
    			System.out.println("Dieses Ticket kostet: " + fahrkartenpreise[9] + "�");
    		}
    		
        System.out.println("Nur 1 bis 10 Tickets!");
        System.out.println("Bei falscher Eingabe wird Anzahl auf 1 gesetzt.");
        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
           
        
        if (anzahlTickets > 10 || anzahlTickets <= 0) { 
        	System.out.println("Falsche Eingabe! Ticketanzahl betr�gt nun 1.");
        	anzahlTickets = 1;
        }        

        return ticketPreis * anzahlTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f  %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
            if(eingeworfenemuenze < 0.5 || eingeworfenemuenze > 2.0) {     	
            	System.out.println("Falscher Eingabewert. Halten Sie sich beim n�chsten Mal bitte an unsere Richtlinien.");
            } //Erkennung ung�ltiger Geldbetr�ge
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");

        
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
               
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Ihr Wechselgeld in Hoehe von %4.2f  %n", rueckgabebetrag);
            System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-muezen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {
    	boolean i = true;
    	do {

        double zuZahlenderBetrag;
        double rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rueckgabebetrag);

 
        System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir wuenschen Ihnen eine angenehme Fahrt.");
        
        
        
        
        	
        
               
       
   
       
    }
    	while (i == true);
    	
   
    
    }
}



