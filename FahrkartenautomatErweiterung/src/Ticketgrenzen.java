//Aufgabe A4.2:
//Der Fahrkartenautomat dahingehend verbessert werden, so dass der Automat nur einen Wert zwischen 1 bis 10 f�r die Anzahl 
//der Tickets annimmt. Gibt der Benutzer einen ung�ltigen Wert ein, soll das Programm mit dem Standardwert (anzahl = 1) 
//weitermachen und dies dem Benutzer durch eine aussagekr�ftige Fehlermeldung mitteilen. Dies soll ebenso bei negativen 
//Ticketpreisen gelten.

import java.util.Scanner;

class Fahrkartenautomat {

    public static double fahrkartenbestellungErfassen() {
        int anzahlTickets;  
        double ticketPreis;
        Scanner tastatur = new Scanner(System.in);
        
        System.out.println("Ticketpreise nur im Posotiv-Bereich, bitte!");
        System.out.print("Ticketpreis (EURO-Cent): ");
        ticketPreis = tastatur.nextDouble();
        
        if (ticketPreis <= 0) {    //Ticketpreis festgelegt, Fehlermeldung erstellt und Preis auf 1 gesetzt.
        	System.out.print("Ticketpreis nur im Positiv-Bereich! Ticketpreis betr�gt nun 1 �.");
        	ticketPreis = 1;
        }
        
        System.out.println("Nur 1 bis 10 Tickets!");
        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
           
        
        if (anzahlTickets > 10 || anzahlTickets <= 0) {   //Anzahl Tickets festlegen, Fehlermeldung erstellt und Anzahl auf 1 gesetzt.
        	System.out.println("Nur 1 bis 10 Tickets! Ticketanzahl betr�gt nun 1.");
        	anzahlTickets = 1;
        }        

        return ticketPreis * anzahlTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f  %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der Rueckgabebetrag in Hoehe von %4.2f  %n", rueckgabebetrag);
            System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-muezen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {

        double zuZahlenderBetrag;
        double rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rueckgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir wuenschen Ihnen eine gute Fahrt.");
    }
}