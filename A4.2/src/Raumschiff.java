import java.util.ArrayList;

public class Raumschiff {

  private int photonentorpedoAnzahl;
  private int energieversorgungInProzent;
  private int schildeInProzent;
  private int huelleInProzent;
  private int lebenserhaltungssystemeInProzent;
  private int androidenAnzahl;
  private String schiffsname;
  private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
  private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
  
  
  // Konstruktoren
  public Raumschiff() {
    
  }
  
  public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent,
                int zustandSchildeInProzent, int zustandHuelleInProzent,
                int zustandLebenserhaltungssystemeInProzent,
                int anzahlDroiden, String schiffsname) {
    this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    this.energieversorgungInProzent = energieversorgungInProzent;
    this.schildeInProzent = zustandSchildeInProzent;
    this.huelleInProzent = zustandHuelleInProzent;
    this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
    this.androidenAnzahl = anzahlDroiden;
    this.schiffsname = schiffsname;
  }
  
  
  // getter & setter
  public int getPhotonentorpedoAnzahl() {
    return photonentorpedoAnzahl;
  }

  public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
    this.photonentorpedoAnzahl = photonentorpedoAnzahl;
  }

  public int getEnergieversorgungInProzent() {
    return energieversorgungInProzent;
  }

  public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
    this.energieversorgungInProzent = energieversorgungInProzent;
  }

  public int getSchildeInProzent() {
    return schildeInProzent;
  }

  public void setSchildeInProzent(int schildeInProzent) {
    this.schildeInProzent = schildeInProzent;
  }

  public int getHuelleInProzent() {
    return huelleInProzent;
  }

  public void setHuelleInProzent(int huelleInProzent) {
    this.huelleInProzent = huelleInProzent;
  }

  public int getLebenserhaltungssystemeInProzent() {
    return lebenserhaltungssystemeInProzent;
  }

  public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
    this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
  }

  public int getAndroidenAnzahl() {
    return androidenAnzahl;
  }

  public void setAndroidenAnzahl(int androidenAnzahl) {
    this.androidenAnzahl = androidenAnzahl;
  }
  public String getSchiffsname() {
    return schiffsname;
  }

  public void setSchiffsname(String schiffsname) {
    this.schiffsname = schiffsname;
  }
  
  
  // methoden
  public void zustandRaumschiff() {
	  System.out.println("Photonentorpedoanzahl:" + photonentorpedoAnzahl);
	  System.out.println("Enegieversorgung in Prozent:" + energieversorgungInProzent);
	  System.out.println("Schilde in Prozent:" + schildeInProzent);
	  System.out.println("H�lle in Prozent:" + huelleInProzent);
	  System.out.println("Lebenserhaltungssysteme in Prozent:" + lebenserhaltungssystemeInProzent);
	  System.out.println("Androiden-Anzahl:" + androidenAnzahl);
	  System.out.println("Schiffsname:" + schiffsname);
	  System.out.println("Broadcast-Kommunikator:" + broadcastKommunikator);
	  System.out.println("Ladungsverzeichnis" + ladungsverzeichnis);

  }
  
  public void printLadungsverzeichnis() {
	  System.out.println("Ladungsverzeichnis" + ladungsverzeichnis);
  }
  
  public void addLadung(Ladung neueLadung) {
    ladungsverzeichnis.add(neueLadung);
  }
  
  public void photonentorpedoSchiessen(Raumschiff r) {
	  if (photonentorpedoAnzahl == 0) {
		  System.out.println("-=*Click*=-");
	      }else {
			  photonentorpedoAnzahl --;
			  System.out.println("Photonentorpedo abgeschossen");
			  treffer(r);
		  }
		  
	  }
  
  
  
  public void phaserkanoneSchiessen(Raumschiff r) {
	  if (energieversorgungInProzent < 50 ) {
		  System.out.println("-=*Click*=-");
	  }else {
		  energieversorgungInProzent =- 50;
		  System.out.println("Phaserkanone abgeschossen");
		  treffer(r);
	  }
  }
  
  private void treffer(Raumschiff r) {
	  System.out.println(schiffsname + "wurde getroffen");
  }
  
  public void nachrichtAnAlle(String message) {
	  System.out.println("Nachricht:" + broadcastKommunikator);
  }
  
  
  public void photonentorpedosLaden(int anzahlTorpedos) {
    
  }
  
  public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,
                                      boolean schiffshuelle, int anzahlDroiden)  {
    
  }
  
  public void ladungsverzeichnisAusgeben() {
    
  }
  
  public void ladungsverzeichnisAufraeumen() {
    
  }
}
