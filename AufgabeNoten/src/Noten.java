//Aufgabe: Erstellen Sie die Konsolenanwendung Noten. Das Programm Noten soll nach der Eingabe
//einer Ziffer die sprachliche Umschreibung ausgeben (1 = Sehr gut, 2 = Gut, 3 = Befriedigend,
//4 = Ausreichend, 5 = Mangelhaft, 6 = Ungen�gend). Falls eine andere Ziffer eingegeben
//wird, soll ein entsprechender Fehlerhinweis ausgegeben werden. 

import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
		System.out.println("Hier k�nnen Sie Ihre Note in die sprachliche Umschreibung ausgegeben lassen.");
		Scanner myScanner = new Scanner(System.in); 
		System.out.println("Geben Sie bitte Ihre Note ein (ganze Zahl von 1 bis 6):");
		int zahl = myScanner.nextInt();
		if (zahl == 1) {
			System.out.println("Sprachliche Umschreibung: Sehr gut.");
		}
		if (zahl == 2) {
			System.out.println("Sprachliche Umschreibung: Gut.");
		}
		if (zahl == 3) {
			System.out.println("Sprachliche Umschreibung: Befriedigend.");
		}
		if (zahl == 4) {
			System.out.println("Sprachliche Umschreibung: Ausreichend.");
		}
		if (zahl == 5) {
			System.out.println("Sprachliche Umschreibung: Mangelhaft.");
		}
		if (zahl == 6) {
			System.out.println("Sprachliche Umschreibung: Ungen�gend.");
		}
		if (zahl < 1 || zahl > 6) {
			System.out.print("Fehler. Es sind nur ganze Zahlen von 1 bis 6 zugelassen. Bitte versuchen Sie es nochmal!");
		}
		System.out.println("Vielen Dank f�r Ihre Teilnahme!");
		
		

	}

}
