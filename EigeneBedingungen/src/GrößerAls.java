//Wenn die 2. Zahl gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben werden 

import java.util.Scanner;

public class Gr��erAls {

	public static void main(String[] args) {
		System.out.println("Ist Ihre 2. Zahl gr��er als die 1.? Wenn ja, bekommen Sie eine Meldung.");
		
		Scanner myScanner = new Scanner(System.in); 
		System.out.print("\nBitte geben Sie eine Zahl ein: "); 
		double zahl1 = myScanner.nextDouble(); 
		System.out.print("\nBitte geben Sie eine weitere Zahl ein: "); 
		double zahl2 = myScanner.nextDouble(); 
		
		if (zahl2 > zahl1) {
			System.out.println("Ihre 2. Zahl ist gr��er als Ihre 1.!");
		}
		
		

	}

}
