//Geben Sie die gr��te der 3 Zahlen aus. (If-Else mit && / Und)

import java.util.Scanner;

public class DreiZahlen3 {

	public static void main(String[] args) {
		System.out.println("Ist Ihre 1. Zahl gr��er oder gleich als die 2. Zahl?");
		
		Scanner myScanner = new Scanner(System.in); 
		System.out.print("\nBitte geben Sie eine Zahl ein: "); 
		double zahl1 = myScanner.nextDouble(); 
		System.out.print("\nBitte geben Sie eine weitere Zahl ein: "); 
		double zahl2 = myScanner.nextDouble(); 
		System.out.print("\nBitte geben Sie eine weitere Zahl ein: "); 
		double zahl3 = myScanner.nextDouble();
		
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Die gr��te Zahl ist: " + zahl1);
		}
		else if (zahl2 > zahl1 && zahl2 > zahl3){
			System.out.println("Die gr��te Zahl ist: " + zahl2);
		}
		else if (zahl3 > zahl1 && zahl3 > zahl2){
			System.out.println("Die gr��te Zahl ist: " + zahl3);
		}
		
		

	}

}
