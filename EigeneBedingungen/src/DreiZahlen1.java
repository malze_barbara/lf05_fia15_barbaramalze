//Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung
//ausgegeben werden (If mit && / Und)

import java.util.Scanner;

public class DreiZahlen1 {

	public static void main(String[] args) {
		System.out.println("Ist Ihre 1. Zahl gr��er als die 2. und 3. Zahl? Wenn ja, bekommen Sie eine Meldung!");
		
		Scanner myScanner = new Scanner(System.in); 
		System.out.print("\nBitte geben Sie eine Zahl ein: "); 
		double zahl1 = myScanner.nextDouble(); 
		System.out.print("\nBitte geben Sie eine weitere Zahl ein: "); 
		double zahl2 = myScanner.nextDouble(); 
		System.out.print("\nBitte geben Sie noch eine weitere Zahl ein: "); 
		double zahl3 = myScanner.nextDouble(); 
		
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Ihre 1. Zahl ist gr��er als die 2. und 3. Zahl!");
		}
	
		

	}

}
