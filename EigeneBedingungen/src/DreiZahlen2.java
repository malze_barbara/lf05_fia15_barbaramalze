//Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung
//ausgegeben werden (If mit || / Oder)

import java.util.Scanner;

public class DreiZahlen2 {

	public static void main(String[] args) {
		System.out.println("Ist Ihre 3. Zahl gr��er als die 2. oder die 1. Zahl? Wenn ja, bekommen Sie eine Meldung!");
		
		Scanner myScanner = new Scanner(System.in); 
		System.out.print("\nBitte geben Sie eine Zahl ein: "); 
		double zahl1 = myScanner.nextDouble(); 
		System.out.print("\nBitte geben Sie eine weitere Zahl ein: "); 
		double zahl2 = myScanner.nextDouble(); 
		System.out.print("\nBitte geben Sie noch eine weitere Zahl ein: "); 
		double zahl3 = myScanner.nextDouble(); 
		
		if (zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println("Ihre 3. Zahl ist gr��er als die 2. oder 3. Zahl!");
		}

	}

}
