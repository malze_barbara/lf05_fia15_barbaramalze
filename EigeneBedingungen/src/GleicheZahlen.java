//Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden
import java.util.Scanner; 

public class GleicheZahlen {

	public static void main(String[] args) {
		System.out.println("Hier k�nnen Sie �berpr�fen, ob Ihre zwei Zahlen identisch sind. Wenn ja, bekommen Sie eine Meldung.");
		
		Scanner myScanner = new Scanner(System.in); 
		System.out.print("\nBitte geben Sie eine ganze Zahl ein: "); 
		double zahl1 = myScanner.nextDouble(); 
		System.out.print("\nBitte geben Sie eine weitere ganze Zahl ein: "); 
		double zahl2 = myScanner.nextDouble(); 
		
		if (zahl1 == zahl2) {
			System.out.println("Die beiden Zahlen sind identisch!");
		}

	}

}
