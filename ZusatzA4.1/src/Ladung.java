//Barbara Malze
public class Ladung {

	//Attribute:
	private String bezeichnung;
	private int menge;
	
	//Konstruktoren
	public Ladung(){
	}
	
	public Ladung(String bezeichnung, int menge) 
	{
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}
	
	//Getter/ Setter:
	public void setBezeichnung(String name) 
	{
		this.bezeichnung = name;
	}
	public String getBezeichnung()
	{
		return this.bezeichnung;
	}
	
	public void setMenge(int menge) 
	{
		this.menge = menge;
	}
	public int getMenge()
	{
		return this.menge;
	}
	
	//Methode:
	public String toString(String args) {
		return args;
		
	}
}