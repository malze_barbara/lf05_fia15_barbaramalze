//Barbara Malze
import java.util.ArrayList;
public class Raumschiff {
	
	//Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList ladungsverzeichnis;
	
	//Konstruktoren
	public Raumschiff()
	{		
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) 
	{
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchildeInProzent(schildeInProzent);
		setHuelleInProzent(huelleInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		setSchiffsname(schiffsname);
		
	}
	
	//Verwaltungsmethoden
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) 
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}
	public int getPhotonentorpedoAnzahl() 
	{
		return this.photonentorpedoAnzahl;
	}	
	
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu)
	{
		this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	}
	
	public int getEnergieversorgungInProzent() 
	{
		return this.energieversorgungInProzent;
	}
	
	public void setSchildeInProzent(int zustandSchildeInProzentNeu)
	{
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}
	public int getSchildeInProzent()
	{
		return this.schildeInProzent;
	}
	
	public void setHuelleInProzent(int zustandHuelleInProzentNeu)
	{
		this.huelleInProzent = zustandHuelleInProzentNeu;
	}
	public int getHuelleInProzent()
	{
		return this.huelleInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu)
	{
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzentNeu;
	}
	public int getLebenserhaltungssystemeInProzent()
	{
		return this.lebenserhaltungssystemeInProzent;
	}
	
	public void setAndroidenAnzahl(int androidenAnzahl)
	{
		this.androidenAnzahl = androidenAnzahl;
	}
	public int getAndroidenAnzahl()
	{
		return this.androidenAnzahl;
	}
	
	public void setSchiffsname(String schiffsname)
	{
		this.schiffsname = schiffsname;
	}
	public String getSchiffsname()
	{
		return this.schiffsname;
	}
		
	//addLadung
	public void addLadung(String args) {
		ArrayList<String> ArrayList = new ArrayList<String>();
		ArrayList.add("Ladung");
	}
	
	//Methodenköpfe
	public void photonentorpedoSchiessen(String args) {
	}
	public void phaserkanonenSchiessen(String args) {
	}
	private void treffer(String args) {
	}
	public void nachrichtAnAlle(String args) {
		String message;
	}
	public void zustandRaumschiff() {
	}
	public void ladungsverzeichnisAusgeben(String args) {
	}

}
	
	
