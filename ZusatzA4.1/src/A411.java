/*Aufgabe: Instanziieren Sie in der Mainmethode verschiedene Raumschiffe und Ladungen, wie im
Objektdiagramm abgebildet. F�gen Sie die entsprechenden Ladungen den Raumschiffen hinzu. */

//Mainmethode
public class A411 {
	
	//Raumschiff 1
	public void KlingonenRaumschiff() {
		int photonentorpedoAnzahl = 1;
		int energieversorgungInProzent = 100;
		int schildeInProzent = 100;
		int huelleInProzent = 100;
		int lebenserhaltungssystemeInProzent = 100;
		String schiffsname = "IKS Hegh'ta";
		int androidenAnzahl = 2;
		
		//Ladung 1 zu Raumschiff 1
		Object ladung1;
		String bezeichnung1 = "Ferengi Schneckensaft";
		int menge1 = 200;
		ladung1 = bezeichnung1 + menge1;
		
		//Ladung 2 zu Raumschiff 1
		Object ladung2;
		String bezeichnung2 = "Bat'leth Klingonen Schwert";
		int menge2 = 200;
		ladung2 = bezeichnung2 + menge2;
	}
	
	//Raumschiff 2
	public void RomulanerRaumschiff() {
		int photonentorpedoAnzahl = 2;
		int energieversorgungInProzent = 100;
		int schildeInProzent = 100;
		int huelleInProzent = 100;
		int lebenserhaltungssystemeInProzent = 100;
		String schiffsname = "IRW Khazara";
		int androidenAnzahl = 2;
		
		//Ladung 1 zu Raumschiff 2
		Object ladung1;
		String bezeichnung1 = "Borg-Schrott";
		int menge1 = 5;
		ladung1 = bezeichnung1 + menge1;
		
		//Ladung 2 zu Rumschiff 2
		Object ladung2;
		String bezeichnung2 = "Rote Materie";
		int menge2 = 2;
		ladung2 = bezeichnung2 + menge2;
		
		//Ladung 3 zu Raumschiff 2
		Object ladung3;
		String bezeichnung3 = "Plasma-Waffe";
		int menge3 = 50;
		ladung3 = bezeichnung3 + menge3;	
	}
	
	//Raumschiff 3
	public void VulkanierRaumschiff() {
		int photonentorpedoAnzahl = 0;
		int energieversorgungInProzent = 80;
		int schildeInProzent = 80;
		int huelleInProzent = 50;
		int lebenserhaltungssystemeInProzent = 100;
		String schiffsname = "Ni'Var";
		int androidenAnzahl = 5;
		
		//Ladung 1 zu Raumschiff 3
		Object ladung1;
		String bezeichnung1 = "Forschungssonde";
		int menge1 = 35;
		ladung1 = bezeichnung1 + menge1;
		
		//Ladung 2 zu Raumschiff 3
		Object ladung2;
		String bezeichnung2 = "Photonentorpedo";
		int menge2 = 35;
		ladung2 = bezeichnung2 + menge2;
	}
}
//Barbara Malze
