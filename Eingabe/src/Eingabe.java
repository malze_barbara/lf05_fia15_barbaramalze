import java.util.Scanner; 

public class Eingabe {

	public static void main(String[] args) {
		 //Aufgabe1: Programm um weitere Grundrechenarten erweitern
		 Scanner myScanner = new Scanner(System.in); 
		 System.out.print("Dies ist ein Rechner."); 
		 System.out.print("\nZuerst beginnen wir mit der Addition!"); //Addition
		 System.out.print("\nBitte geben Sie eine ganze Zahl ein: "); 
		 int zahl1 = myScanner.nextInt(); 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 int zahl2 = myScanner.nextInt(); 
		 int ergebnis = zahl1 + zahl2;
		 System.out.print("\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis); 
		 
		 System.out.print("\nNun zur Subtraktion!"); //Subtraktion
		 System.out.print("\nBitte geben Sie eine ganze Zahl ein: "); 
		 int zahl1_2 = myScanner.nextInt(); 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 int zahl2_2 = myScanner.nextInt(); 
		 int ergebnis_2 = zahl1_2 - zahl2_2;
		 System.out.print("\nErgebnis der Subtraktion lautet: ");
		 System.out.print(zahl1_2 + " - " + zahl2_2 + " = " + ergebnis_2); 
		 
		 System.out.print("\nNun zur Multiplikation!"); //Multiplikation
		 System.out.print("\nBitte geben Sie eine ganze Zahl ein: "); 
		 int zahl1_3 = myScanner.nextInt(); 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 int zahl2_3 = myScanner.nextInt(); 
		 int ergebnis_3 = zahl1_3 * zahl2_3;
		 System.out.print("\nErgebnis der Multiplikation lautet: ");
		 System.out.print(zahl1_3 + " * " + zahl2_3 + " = " + ergebnis_3); 
		 
		 System.out.print("\nNun zur Division!"); //Division
		 System.out.print("\nBitte geben Sie eine ganze Zahl ein: "); 
		 int zahl1_4 = myScanner.nextInt(); 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 int zahl2_4 = myScanner.nextInt(); 
		 int ergebnis_4 = zahl1_4 / zahl2_4;
		 System.out.print("\nErgebnis der Division lautet: ");
		 System.out.print(zahl1_4 + " / " + zahl2_4 + " = " + ergebnis_4); 
		 
		 myScanner.close();
	 }
}
	



